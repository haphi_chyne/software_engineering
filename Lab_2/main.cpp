#include <iostream>
using namespace std;

#include "functions.h"

int main(void)
{
    int heap[50],el, n=0, ch;

    while (1)
    {
        system("cls");
        if(n > 0)
        cout << "\n Array in heap: ";
        for (int i = 0; ((i < n) && (n > 0)); i++)
            cout << heap[i] << "  ";
        cout << "\n \nEnter your choice: "<<endl;
        cout << "\n 1. Insert Element\t 2. Delete Element\t 3. exit\n Choice: ";
        cin >> ch;
        switch (ch)
        {
        case 1:
            cout << "Enter element: ";
            cin >> el;
            n++;
            InsertElement(heap,el, n);
            break;

        case 2:
            cout << "Enter element: ";
            cin >> el;
            DeleteElement(heap, el, &n);
            break;

        case 3:
            exit(0);
            break;

        default:
            cout << "\nEnter the correct choice!"<<endl;
            break;
        }
    }
    return (0);
}
