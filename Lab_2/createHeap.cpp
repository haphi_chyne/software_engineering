#include "functions.h"

void createHeap(int heap[], int node)
{   
      int parent, tmp;
      if (node!=0) {  
            parent= (int)node/ 2;
            if (heap[parent] < heap[node]) {
                  tmp = heap[parent];
                  heap[parent] = heap[node];
                  heap[node] = tmp;

                  createHeap(heap,parent);
            }
      }
}