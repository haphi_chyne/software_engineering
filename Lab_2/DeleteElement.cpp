#include <iostream>

using namespace std;
#include "functions.h"

void DeleteElement(int heap[], int value, int* n)
{
    int left, right, i, temp, parentnode;
 
    for (i = 0; i < *n; i++) {
        if (value == heap[i])
            break;
    }
    if (value != heap[i])
    {
        cout<<value<<" not found in heap list\n";
    }
    heap[i] = heap[*n - 1];
    *n = *n - 1;
    parentnode =(i - 1) / 2; /*find parentnode of node i */
    if (heap[i] > heap[parentnode])
    {
        InsertElement(heap,heap[i], i);
        return;
    }
    left = 2 * i + 1; /*left child of i*/
    right = 2 * i + 2; /* right child of i*/
    while (right < *n)
    {
        if (heap[i] >= heap[left] && heap[i] >= heap[right])
            return;
        if (heap[right] <= heap[left])
        {
            temp = heap[i];
            heap[i] = heap[left];
            heap[left] = temp;
            i = left;
        }
        else
        {
            temp = heap[i];
            heap[i] = heap[right];
            heap[right] = temp;
            i = right;
        }
        left = 2 * i + 1;
        right = 2 * i + 2;
    }/*End of while*/
    if (left == *n - 1 && heap[i])    {
        temp = heap[i];
        heap[i] = heap[left];
        heap[left] = temp;
    }
}