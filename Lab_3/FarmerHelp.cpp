#include<string>
using namespace std;

class FarmerHelp
{
    float idealRain;
    string Month,cropType;

    public:
    FarmerHelp(string crop, float rain){
        cropType = crop;
        idealRain = rain;
        cout<<endl<<"Crop - "<<cropType;
        cout<<endl<<"Ideal Rain - "<<idealRain;
    }

    string farmerSuggest()
    {
        float avg;
        for (int i = 0; i < 12; i++)
        {
            avg = (weather::getInstance()->getRain(i) + weather::getInstance()->getRain(i + 12)) / 2;
            if (avg > idealRain - 5 && avg < idealRain + 5)
            {
                return weather::getInstance()->getMonth(i);
            }
        }
    }
};