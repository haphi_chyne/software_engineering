#include <string>
using namespace std;

class TravelPlanner
{
    int season;
    int Month;

  public:
    TravelPlanner(string seasonType)
    {
        if (seasonType == "summer")
            season = 1;
        if (seasonType == "winter")
            season = 2;
        if (seasonType == "rainy")
            season = 3;
        cout<<endl<<"Travel - "<<seasonType;
    }
    string travelSuggest()
    {
        int max = 0, min = 999;
        if (season == 1)
        {
            for (int i = 0; i < 24; i++)
            {
                if (weather::getInstance()->getTemp(i) > max)
                {
                    Month = i;
                    max = weather::getInstance()->getTemp(i);
                }
            }
        }

        else if (season == 2)
        {
            for (int i = 0; i < 24; i++)
            {
                if (weather::getInstance()->getTemp(i) < min)
                {
                    Month = i;
                    min = weather::getInstance()->getTemp(i);
                }
            }
        }

        else if (season == 3)
        {
            for (int i = 0; i < 24; i++)
            {
                if (weather::getInstance()->getRain(i) > max)
                {
                    Month = i;
                    max = weather::getInstance()->getRain(i);
                }
            }
            
        }
        return weather::getInstance()->getMonth(Month);
    }
};