#include <iostream>

using namespace std;

class weather
{
  private:
    string month[12];
    float temp[24];
    float rain[24];
    static weather *weatherInstance;

    weather(){
        init_values();
        month[0] = "January";
        month[1] = "February";
        month[2] = "March";
        month[3] = "April";
        month[4] = "May";
        month[5] = "June";
        month[6] = "July";
        month[7] = "August";
        month[8] = "September";
        month[9] = "October";
        month[10] = "November";
        month[11] = "December";
    }
    

  public:
    static weather* getInstance(){
        if(!weatherInstance){
            weatherInstance = new weather;
        }
        return weatherInstance;
    }
    
    float getTemp(int i)
    {
        return temp[i - 1];
    }

    float getRain(int i)
    {
        return rain[i - 1];
    }

    void setTemp(int i, float value)
    {
        temp[i - 1] = value;
    }
    void setRain(int i, float value)
    {
        rain[i - 1] = value;
    }

    void init_values()
    {
        for (int i = 0; i < 24; i++)
        {
            if(i<3 || (i>10 && i<15)||i==23)
            {
                temp[i] = 5 + rand() % (19 - 5 + 1);
            rain[i] = 2 + rand() % (10 - 2 + 1);
            }
            if((i>2&&i<7) || (i>14 && i<19))
            {
                temp[i] = 25 + rand() % (45 - 25 + 1);
                rain[i] = 0 + rand() % (5 - 0 + 1);
            }
            if((i>6&&i<11) || (i>18 && i<23))
            {
                temp[i] = 10 + rand() % (23 - 10 + 1);
                rain[i] = 15 + rand() % (25 - 15 + 1);
            }
        }
    }

    string getMonth(int m)
    {
        return month[m];
    }
    
    void Display()
    {
        cout << "\t\tTemp\tRainfall" << endl;
        for (int i = 0; i < 24; i++)
        {
            cout <<"Month "<<(i + 1 )%13<< ":\t" << temp[i] << "\t" << rain[i] << endl;
        }
    }
};

weather* weather::weatherInstance = 0;
