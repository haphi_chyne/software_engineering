#include <iostream>
#include <string>
#include <cstdlib>
using namespace std;

class Button
{
    protected:
        std::string type;

    public:
        virtual Button* clone() = 0; 

        std::string getType()
        {
            return type;
        }

};

class CheckBox : public Button
{
    public:
        CheckBox()
        {
            type  = "Check-box";
        }
        
        Button* clone()
        {
            return new CheckBox(*this);
        }
};

class RadioButton : public Button
{
    public:
        RadioButton()
        {
            type  = "Radio Button";
        }

        Button* clone()
        {
            return new RadioButton(*this);
        }
};

class ToggleButton : public Button
{
    public:
        ToggleButton()
        {
            type  = "Toggle Button";
        }

        Button* clone()
        {
            return new ToggleButton(*this);
        }
};


class ButtonFactory
{
    static Button* type1;
    static Button* type2;
    static Button* type3;

    public:
        static void  loadButton()
        {
            type1 = new CheckBox();
            type2 = new RadioButton();
            type3 = new ToggleButton();
        }

        static Button* getButton(int ch)
        {

	if(ch==1)
            return type1->clone();

	else if(ch==2)        
            return type2->clone();
       
	else if(ch==3)
            return type3->clone();
        }
};

Button* ButtonFactory::type1 = 0;
Button* ButtonFactory::type2 = 0;
Button* ButtonFactory::type3 = 0;