#include <iostream>
#include <string>
#include <cstdlib>
#include "Buttons.cpp"

using namespace std;


void operation()
{
    Button* object;
	
	int choice;
	std::cout<<"1. Check Box\t 2. Radio Button\t 3. Toggle Button\n>>";
    cin>>choice;

    object = ButtonFactory::getButton(choice);
    std::cout << std::endl<< object->getType()<< std::endl;
}


int main()
{
	int i;
	ButtonFactory::loadButton();
	
 	while(1)
	{
		std::cout<<"1. Choose Button \n2. Exit\n >> ";
		std::cin>>i;
		if(i==2)
		exit(0);
		else
		operation();
	}

    return 0;
}